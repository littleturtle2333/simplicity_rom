# 更多拓展模块

#### Magisk框架
2022.05.01 **之后** 的 SimplicityROM 已内置 Magisk 正式版。  
 **Alpha管理器与Magisk管理器无法相互兼容** 
如果您的管理器 丢失/无法识别或异常，请卸载管理器后下载此处的管理器进行恢复。  
下载地址：https://lt2333.lanzouw.com/b0dxymk4h 密码:1234  

#### Alpha框架（Magisk的衍生版）
2022.05.01 **之前** 的 SimplicityROM 已内置 Alpha 版。  
 **Alpha管理器与Magisk管理器无法相互兼容** 
如果您的管理器 丢失/无法识别或异常，请卸载管理器后下载此处的管理器进行恢复。  
下载地址：https://lt2333.lanzouw.com/b0dxsvx1a 密码:1234  


#### LSPosed框架（依赖Magisk-Zygisk）
LSPosed框架 是一个类Xposed框架，可以在不直接修改任何应用和系统组件的情况下达到修改的目的，从而实现强大的功能。  
下载地址：https://lt2333.lanzouw.com/b0dxp8izi 密码:1234  
第一步：下载LSPosed安装文件，  
第二步：打开【Magisk】设置，开启Zygisk功能，  
第三步：点击【模块】，从本地安装，选择下载的安装文件  
完成！

#### Simplicity Tools（依赖LSPosed框架）
 **Simplicity** 官方开发 的一个基于  **MIUI13（Android12）** 适配的系统拓展Xposed模块  
 **仅支持MIUI13（Android12）** 。其余版本无效，请不要安装使用。  
![输入图片说明](images/cn.jpg)  
下载地址：https://www.coolapk.com/apk/com.lt2333.simplicitytools  
第一步：安装APK，  
第二步：打开【LSPosed管理器】-> 模块 -> 激活模块  
完成！

#### Shamiko （依赖Magisk-Zygisk）
Shamiko 是一个由 LSPosed团队 开发的一款闭源的防止检测ROOT模块，效果极佳。  
下载地址：https://lt2333.lanzouw.com/b0dxpci1i 密码:1234  
第一步：下载Shamiko安装文件，  
第二步：打开【Magisk】设置，开启Zygisk功能， **关闭遵循排除列表**   
第三步：点击【模块】，从本地安装，选择下载的安装文件  
第四步：打开【Magisk】设置->配置排除列->加入防止被检测的软件  
完成！

#### MIUIHome（依赖LSPosed）
针对 Miui 桌面的自定义扩展
![输入图片说明](images/MiuiHome.png)  
可在LSPosed仓库自行下载

#### 墨·状态栏歌词（依赖LSPosed）
自动获取音乐软件当前播放的歌曲，并实时将歌词显示在状态栏  
![输入图片说明](images/1ffd99b8e0cb483f1d3d9445a309a2b3.jpg)  
下载地址：https://www.coolapk.com/apk/statusbar.lyric

#### MIUI 原生通知图标（依赖LSPosed）
修复被 MIUI 开发组丢弃的原生通知图标，并完美地给 MIUI 修复了黑白块图标的问题。
![输入图片说明](images/MIUINativeNotifyIcon.jpg)  
下载地址：https://ghproxy.com/https://github.com/fankes/MIUINativeNotifyIcon/releases/download/2.5/app-release.apk

#### 其他
可自行在LSPosed仓库寻找适合您的模块
